#!/usr/bin/python3
import argparse
import os
import functools
from enum import Enum

import git
import semver


class VersionType(Enum):
    Major = "major"
    Minor = "minor"
    Patch = "patch"
    Prerelease = "prerelease"
    Build = "build"

    def __str__(self):
        return self.value

    def bump(self, semver_version: semver.Version) -> semver.Version:
        if self == VersionType.Major:
            return semver_version.bump_major()
        if self == VersionType.Minor:
            return semver_version.bump_minor()
        if self == VersionType.Patch:
            return semver_version.bump_patch()
        if self == VersionType.Prerelease:
            return semver_version.bump_prerelease()
        if self == VersionType.Build:
            return semver_version.bump_build()


def parse_input():
    parser = argparse.ArgumentParser(description="Next tag")
    parser.add_argument("type", help="Version type", type=VersionType, choices=list(VersionType))
    parser.add_argument("-d", "--directory",
                        help="Git repository directory",
                        default=os.getcwd())
    args = parser.parse_args()

    repo = git.Repo(args.directory)
    latest_tag = repo.git.describe("--tags")
    latest_tag_semver = semver.Version.parse(latest_tag)

    new_tag = args.type.bump(latest_tag_semver)
    repo.create_tag(new_tag)

    print(f"New tag {new_tag} created")


if __name__ == "__main__":
    parse_input()
