# next-tag

Get the latest semver tag and bump it

## Usage

```
grrfe@gitlab:~$ next-tag --help
usage: next-tag [-h] [-d DIRECTORY] {major,minor,patch,prerelease,build}

Next tag

positional arguments:
  {major,minor,patch,prerelease,build}
                        Version type

optional arguments:
  -h, --help            show this help message and exit
  -d DIRECTORY, --directory DIRECTORY
                        Git repository directory

```
